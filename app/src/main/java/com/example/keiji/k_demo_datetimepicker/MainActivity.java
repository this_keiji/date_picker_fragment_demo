package com.example.keiji.k_demo_datetimepicker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;


public class MainActivity extends AppCompatActivity {

    // Constants
    final static Calendar CAL = Calendar.getInstance();

    // date vars
    private static int aYear;
    private static int aMonth;
    private static int aDay;


    /**
     * CONSTRUCTOR
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }//--end of onCreate METHOD


    /**
     * Open a fragment with the calendar
     * @param view
     */
    public void myOpenCalendar (View view) {

        // instanciate the date picker fragment
        DatePickerFragment dpFragment = new DatePickerFragment();

        // show the fragment
        dpFragment.show(getSupportFragmentManager(), "datePicker");

    }//--end of myOpenCalendar METHOD



    /*********************************** ADDITIONAL CLASSES *************************************/
    /**
     * Fragment to deal with date picker
     */
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        /**
         * onCreateDialog() set date properties to current day and return a Datepicker dialog
         * @param savedInstanceState
         * @return Dialog
         */
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            // Use the current date as the default date in the picker
            aYear  = CAL.get(Calendar.YEAR);
            aMonth = CAL.get(Calendar.MONTH);
            aDay   = CAL.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, aYear, aMonth, aDay);

        }


        /**
         * onDateSet() sets the SaveMe date properties to the picked date
         * @param view datepicker view
         * @param year year picked in datepicker view
         * @param month month picked in datepicker view
         * @param day day picked in datepicker view
         */
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {

            String strYear = Integer.toString(year);
            String strMonth = Integer.toString(month + 1);
            String strDay = Integer.toString(day);

            // display toast
            Toast.makeText(getActivity(), "year: " + strYear + ", month: " + strMonth + ", day: " + strDay, Toast.LENGTH_LONG).show();

        }


    }//--end of datepickerfragment class


}//--END OF MAIN ACTIVITY
